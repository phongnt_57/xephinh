
package tetri;

// Board.java

import static tetri.Piece.L1_STR;
import static tetri.Piece.L2_STR;
import static tetri.Piece.PYRAMID_STR;
import static tetri.Piece.S1_STR;
import static tetri.Piece.S2_STR;
import static tetri.Piece.SQUARE_STR;

import static tetri.Piece.STICK;
import static tetri.Piece.STICK_STR;


/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
*/
class backup{
    public boolean[][] grids;
    public int[] bwidths;
    public int[] bheights;
    public backup(){
         bwidths = new int[25];
         bheights = new int [25]; 
         grids = new boolean[10][25];
		
                for(int i=0;i<10;i++)
                    for(int j=0;j<25;j++)
                        grids[i][j] = false;
    }
    
}
public class Board	{
	// Some ivars are stubbed out for you:
	private int width;
	private int height;
	private boolean[][] grid;
	private boolean DEBUG = true;
	boolean committed;
        private int[] widths;
        private int[] heights;
        private backup com;
	
	
	// Here a few trivial methods are provided:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	*/
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		grid = new boolean[width][height];
		committed = true;
                for(int i=0;i<width;i++)
                    for(int j=0;j<height;j++)
                        grid[i][j] = false;
                widths = new int[this.height];
                heights = new int [this.width]; 
                com = new backup();
		
		// YOUR CODE HERE
	}
	
	
	/**
	 Returns the width of the board in blocks.
	*/
	public int getWidth() {
		return width;
	}
        public int[] widths(){
           for(int i=0;i<height;i++){
                widths[i] = 0;
                for(int j=0;j<width;j++){
                    if(grid[j][i]) widths[i]++;
                }
           }
            return widths; 
                
            
            
        }
        public int[] heights(){
             for(int i=0;i < width ;i++){
                heights[i] = 0;
                for(int j = 0; j<height ;j++){
                    if(grid[i][j]) heights[i]++;
                }
           
            }
                
        return heights;
        }
	

	/**
	 Returns the height of the board in blocks.
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	*/
	public int getMaxHeight() {
            int i=0;
            int max =heights[0];
            for( i=0;i<heights.length;i++)
                if (max<heights[i]) max = heights[i];
	    return max; // YOUR CODE HERE
	}
	
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	*/
	public void sanityCheck() {
	/*	if (DEBUG) {
			int tmp_maxHeight = 0;
			int tmp_curHeight = 0;
			int tmp_widths[] = new int[height];
			for (int i = 0; i < this.widths.length; i++) {
				tmp_curHeight = 0;				
				for (int j = 0; j < this.heights.length; j++) {
					if (grid[i][j]) {
						tmp_curHeight++;
						tmp_widths[i]++;
					}					
				}
				if (heights[i] != tmp_curHeight)
					throw new RuntimeException("Heights array is corrupted, pos: " + Integer.toString(i));
				tmp_maxHeight = Math.max(tmp_maxHeight, tmp_curHeight);
			}
			if (tmp_maxHeight != maxHeight)
				throw new RuntimeException("MaxHeight is corrupted: is " + Integer.toString(maxHeight) + " should be " + Integer.toString(tmp_maxHeight));
			if (widths != tmp_widths)
				throw new RuntimeException("Widhts array is corrupted");
		}*/
	}
	
	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.
	 
	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	*/
	public int dropHeight(Piece piece, int x) {
           //if(x+4 > width ){return 0;}
            
            int[] s = piece.getSkirt();
            for(int i =0 ;i<s.length;i++)
            {
                int dem=0;
                for( int j=0;j<s.length;j++)
                {
                    if( (s[j] - s[i] + heights[x+i]) >= heights[x+j]) dem++;
                    
                }
                if (dem==s.length) return (heights[i+x]-s[i]);
                
                
            
                 
                     
                
                     
                
                
                
            }
           
             
                
            
            
            
            
          return  (heights[i+x]-s[i]);
		 // YOUR CODE HERE
	}
	
	
	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	*/
	public int getColumnHeight(int x) {
                if(x<this.getWidth())
		return heights[x]; // YOUR CODE HERE
                else return 0;
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	*/
	public int getRowWidth(int y) {
		 return widths[y]; // YOUR CODE HERE
	}
	
	
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	*/
	public boolean getGrid(int x, int y) {
            if (x<width&&y<height)
            return grid[x][y];
            else
		return false; // YOUR CODE HERE
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	*/
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		if (!committed) throw new RuntimeException("place commit problem");
                int i;
                System.out.print("x,y:"+x+" "+y+"\n");
               //if(y>height-3 || y<0) return PLACE_OUT_BOUNDS;
               //if(x>width-3 ||x<0) return PLACE_OUT_BOUNDS;
                
                TPoint[] t = new TPoint[4];
                t = piece.getBody();
                for( i=0;i<t.length;i++)
                {
                   
                    System.out.print("before"+t[i].toString()+"\n");
                    //if(t[i].x>width || t[i].y >height) return  PLACE_OUT_BOUNDS;
                }
                //for( i=0;i<t.length;i++){
                // System.out.print("Toa do cac manh truoc::"+t[i].x+""+t[i].y+"\n");    
                //}
                //int ny = dropHeight(piece,x);
               // System.out.print("Drop:"+ny);
               /* for( i=0;i<t.length;i++){
                 int   a = t[i].x + x ;
                 int    b =t[i].y +ny;
                   System.out.print("Toa do cac manh sau nay::"+a+""+b+"\n");
                    }*/
                if(piece.getWidth()+x > this.getWidth() || piece.getHeight() + y > this.getHeight() ||
                        x < 0 || y < 0) { 
                    System.out.print("out bounds \n");
                    return PLACE_OUT_BOUNDS;
                }
                
                for(i=0;i<t.length;i++)
               {   if(t[i].x +x > width || t[i].y +y> height)
               {   System.out.print("out\n");
             
                   return PLACE_OUT_BOUNDS;
               }
               } 
               
                
                
                for(i=0;i<t.length;i++){
                    System.out.print((t[i].x+x)+" "+(t[i].y+y)+"\n");
                 
                 }
                 for(i=0;i<t.length;i++){
                         if (grid[t[i].x+x][t[i].y+y]==true) { 
                   System.out.print("bad:"+t[i].toString()+"\n");
                   
                   return PLACE_BAD;
                   
               }
               
               }
                for(i=0;i<t.length;i++){
                 if (grid[t[i].x+x][t[i].y+y]== false) grid[t[i].x+x][t[i].y+y] =true;}
              //   for(i=0;i<t.length;i++){
                //     heights[t[i].y]++;
                //     widths[t[i].x]++;
                // }
                
                
                 heights=heights();
                 widths= widths();
                 getMaxHeight();
                  for( i=0;i<width;i++)
                    if(widths[i]==width) return  PLACE_ROW_FILLED;
                System.out.print("ok\n");
               
                 return PLACE_OK;                            
                            
                
                                     
                                          
                            
		 		
	
	}
	
	
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	*/
        public boolean endgame(){
        for(int i=0;i<this.width;i++)
            if(grid[i][19]) return true; 
        return false;
        
        }
	public int clearRows() {
		int rowsCleared = 0;
                for(int i=0;i<height;i++)
                {
                    if(getRowWidth(i)==width){
                        rowsCleared++;
                        for(int z =i;z<= getMaxHeight();z++)
                            for(int j=0;j<width;j++)
                                grid[j][z] = grid[j][z+1];
                   
                }
                }
               heights=heights();
               widths= widths();
               getMaxHeight();
             //  this.commit();
		// YOUR CODE HERE
		sanityCheck();
		return rowsCleared;
	}



	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	*/
        
	public void undo() {
         System.arraycopy( com.bwidths , 0, this.widths, 0, height);
            System.arraycopy(com.bheights , 0, this.heights, 0, width);
            for(int i=0;i<width;i++)
                System.arraycopy(com.grids[i], 0, grid[i] , 0, height);
            this.heights();
                 this.widths();
                 this.getMaxHeight();;
		// YOUR CODE HERE
	}
	
	
	/**
	 Puts the board in the committed state.
	*/
        public void back(){
            System.arraycopy(this.widths, 0, com.bwidths, 0, height);
            System.arraycopy(this.heights, 0, com.bheights, 0, width);
            for(int i=0;i<width;i++)
                System.arraycopy(grid[i], 0, com.grids[i], 0, height);
            
            
           
        }
	public void commit() {
		committed = true;
                 this.back();
                 this.heights();
                 this.widths();
                 this.getMaxHeight();;
             
            
                
                
                
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex
	 state change over time.
	 (provided debugging utility) 
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('p');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append("+");
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}
        public void toAString(){
        for(int i=height-1;i>=0;i--)
        {    for(int j=0;j <width;j++)
            {    if(!grid[j][i]) System.out.print("0");
            else System.out.print("x");}
        System.out.print("\n");
        }
        for(int i=0;i<heights.length;i++)
            System.out.print("height:"+heights[i]+" ");
        System.out.print("\n");
        for(int i=0;i<widths.length;i++)
            System.out.print(widths[i]+" ");
        
        
        }
  public static void main(String args[]){
            Board t = new Board(10,20);
            
            
               
          // Piece a = new Piece(PYRAMID_STR);
          // t.place(a,1 ,15 );
          // t.commit();
         // t.toAString();
         // TPoint[] z = a.getBody();
         // Piece b = a.fastRotation();
         // TPoint[] z1 = b.getBody();
        //   for(int i=0;i<4;i++)
         //     System.out.print(z[i].toString());
           //   System.out.print("\n\n\n");
         // for(int i=0;i<4;i++)
           //   System.out.print(z1[i].toString());
          System.out.print("\n\n\n");
           
          
         //   System.out.print("drop:"+t.dropHeight(b, 1)+"\n");
          //  int[] z3 =b.getSkirt();
         //   for(int i=0;i<z3.length;i++)
          //    System.out.print("skirt:"+z3[i]+"\n")  ;
        //  t.place(b, 1, 15);
          
         // t.toAString();
          System.out.print("\n\n\n");
          Piece d = new Piece(PYRAMID_STR);
         
          Piece e = new Piece(PYRAMID_STR);
          Piece f = new Piece(STICK_STR);
            t.place(d,4,0 );
             t.commit();
            t.place(e,4 ,1 );
            t.place(f,4,3 );
           System.out.print(t.toString());
         
               System.out.print("\n\n\n");
               
               System.out.print(t.clearRows());
            
          t.toAString();
           System.out.print("\n\n\n");
         
           
               t.toAString();
               
                   
                   
           
            
            
           
           
        }
   
}


