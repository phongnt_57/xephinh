/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetri;


// Piece.java

import java.util.*;
import java.lang.Exception;


/**
 An immutable representation of a tetris piece in a particular rotation.
 Each piece is defined by the blocks that make up its body.
 
 Typical client code looks like...
 <pre>
 Piece pyra = new Piece(PYRAMID_STR);		// Create piece from string
 int width = pyra.getWidth();			// 3
 Piece pyra2 = pyramid.computeNextRotation(); // get rotation, slow way
 
 Piece[] pieces = Piece.getPieces();	// the array of root pieces
 Piece stick = pieces[STICK];
 int width = stick.getWidth();		// get its width
 Piece stick2 = stick.fastRotation();	// get the next rotation, fast way
 </pre>
*/

public class Piece {
	// Starter code specs out a few basic things, leaving
	// the algorithms to be done.
	private TPoint[] body;
	private int[] skirt;
	private int width;
	private int height;
	private Piece next; // "next" rotation

	static private Piece[] pieces;	// singleton static array of first rotations

	/**
	 Defines a new piece given a TPoint[] array of its body.
	 Makes its own copy of the array and the TPoints inside it.
	*/
	public Piece(TPoint[] points) {
          // body = new TPoint[points.length];
         
          //      for (int i=0; i < points.length; i++)
            //    {   
               //     body[i] =  TPoint(points[i].x,points[i].y);
             //   }
                // height = width = -1;
                body = new TPoint[points.length];
                for (int i=0; i < points.length; i++)
                body[i] = points[i];    
	}
	

	
	
	/**
	 * Alternate constructor, takes a String with the x,y body points
	 * all separated by spaces, such as "0 0  1 0  2 0	1 1".
	 * (provided)
	 */
	public Piece(String points) {
		this(parsePoints(points));
	}

	/**
	 Returns the width of the piece measured in blocks.
	*/
	public int getWidth() {
            int i, min ,max ;
            min = body[0].x;
            max = body[0].x;
            for( i=1;i<body.length;i++)
              if(max < body[i].x) max = body[i].x;
            for( i=1;i<body.length;i++)
               if(min > body[i].x) min = body[i].x;
            width = max - min +1;
            
                
               
               
               
               
              
		return width;
	}

	/**
	 Returns the height of the piece measured in blocks.
	*/
	public int getHeight() {
            int i, min ,max ;
            min = body[0].y;
            max = body[0].y;
            for( i=1;i<body.length;i++)
              if(max < body[i].y) max = body[i].y;
            for( i=1;i<body.length;i++)
               if(min > body[i].y) min = body[i].y;
          height  = max - min +1;
                
               
               
               
               
             
		return  height;
	}

	/**
	 Returns a pointer to the piece's body. The caller
	 should not modify this array.
	*/
	public TPoint[] getBody() {
		return body;
	}

	/**
	 Returns a pointer to the piece's skirt. For each x value
	 across the piece, the skirt gives the lowest y value in the body.
	 This is useful for computing where the piece will land.
	 The caller should not modify this array.
	*/
	public int[] getSkirt() {
            width = getWidth(); 
            skirt = new int[width];
           
            for(int i=0;i<width;i++)
            {
                for(int j=0;j<4;j++)
                {
                    if(body[j].x==i) skirt[i] =  body[i].y;
                }
                for(int j=0;j<4;j++)
                    if(body[j].x==i && body[j].y < skirt[i] ) skirt[i] = body[j].y;
                
               
            }
         
            
            return skirt;
	}

	
	/**
	 Returns a new piece that is 90 degrees counter-clockwise
	 rotated from the receiver.
	 */
	public Piece computeNextRotation() {
		// YOUR CODE HERE
            List<TPoint> points = new ArrayList<TPoint>();
             int i,j ,k;
            k=0;
            int[][] a,b;
            a= new int[4][4];
            b= new int [4][4];
            
            int c = this.getHeight();
            int h = this.getWidth();
            
          //  TPoint[] t1 = new TPoint[body.length];                 
           
            for( i=0;i<width;i++)
                for( j=0;j<height;j++)
                    a[i][j]=0;
            for( i=0;i < body.length; i++)
             a[body[i].x][body[i].y] =1; 
           
           
           
               for(i=height-1;i>=0;i--)
                     for(j=width-1;j>=0;j--)
        {
           b[i][j]=a[j][height-1-i];
        }
             for(i = 0;i < c;i++)
               for(j=0;j<h;j++)
                    if(b[i][j]==1){
          
                      // System.out.print("Cap i,j:"+i+j+"\n ");
                       points.add(new TPoint(i, j));
                         
                    
                    }
             TPoint[] array = points.toArray(new TPoint[0]);
             return new Piece(array);
            
            
            
                
                
            
		//return null; // YOUR CODE HERE
	}

	/**
	 Returns a pre-computed piece that is 90 degrees counter-clockwise
	 rotated from the receiver.	 Fast because the piece is pre-computed.
	 This only works on pieces set up by makeFastRotations(), and otherwise
	 just returns null.
	*/	
	public Piece fastRotation() {
                rotate2();
		return next;
	}
	


	/**
	 Returns true if two pieces are the same --
	 their bodies contain the same points.
	 Interestingly, this is not the same as having exactly the
	 same body arrays, since the points may not be
	 in the same order in the bodies. Used internally to detect
	 if two rotations are effectively the same.
	*/
	public boolean equals(Object obj) {
		// standard equals() technique 1
		if (obj == this) return true;
		
		// standard equals() technique 2
		// (null will be false)
		if (!(obj instanceof Piece)) return false;
		Piece other = (Piece)obj;
		
		// YOUR CODE HERE
		return true;
	}


	// String constants for the standard 7 tetris pieces
	public static final String STICK_STR	= "0 0	0 1	 0 2  0 3";
	public static final String L1_STR		= "0 0	0 1	 0 2  1 0";
	public static final String L2_STR		= "0 0	1 0 1 1	 1 2";
	public static final String S1_STR		= "0 0	1 0	 1 1  2 1";
	public static final String S2_STR		= "0 1	1 1  1 0  2 0";
	public static final String SQUARE_STR	= "0 0  0 1  1 0  1 1";
	public static final String PYRAMID_STR	= "0 0  1 0  1 1  2 0";
	
	// Indexes for the standard 7 pieces in the pieces array
	public static final int STICK = 0;
	public static final int L1	  = 1;
	public static final int L2	  = 2;
	public static final int S1	  = 3;
	public static final int S2	  = 4;
	public static final int SQUARE	= 5;
	public static final int PYRAMID = 6;
	
	/**
	 Returns an array containing the first rotation of
	 each of the 7 standard tetris pieces in the order
	 STICK, L1, L2, S1, S2, SQUARE, PYRAMID.
	 The next (counterclockwise) rotation can be obtained
	 from each piece with the {@link #fastRotation()} message.
	 In this way, the client can iterate through all the rotations
	 until eventually getting back to the first rotation.
	 (provided code)
	*/
	public static Piece[] getPieces() {
		// lazy evaluation -- create static array if needed
		if (Piece.pieces==null) {
			// use makeFastRotations() to compute all the rotations for each piece
			Piece.pieces = new Piece[] {
				makeFastRotations(new Piece(STICK_STR)),
				makeFastRotations(new Piece(L1_STR)),
				makeFastRotations(new Piece(L2_STR)),
				makeFastRotations(new Piece(S1_STR)),
				makeFastRotations(new Piece(S2_STR)),
				makeFastRotations(new Piece(SQUARE_STR)),
				makeFastRotations(new Piece(PYRAMID_STR)),
			};
		}
		
		
		return Piece.pieces;
	}
	


	/**
	 Given the "first" root rotation of a piece, computes all
	 the other rotations and links them all together
	 in a circular list. The list loops back to the root as soon
	 as possible. Returns the root piece. fastRotation() relies on the
	 pointer structure setup here.
	*/
	/*
	 Implementation: uses computeNextRotation()
	 and Piece.equals() to detect when the rotations have gotten us back
	 to the first piece.
	*/
	private static Piece makeFastRotations(Piece root) {
		// YOUR CODE HERE
		return root;
                
                
                
                
	}
	
	public void rotate2(){
            Piece[] m = new Piece[4];
                m[0] = computeNextRotation();
                m[1] =m[0].computeNextRotation();
                m[2] =m[1].computeNextRotation();
                m[3] =m[2].computeNextRotation();
                if(this.equals(m[3])) next = m[0];
                else
                for(int i=0;i<3;i++){
                    
                if(this.equals(m[i])) next = m[i+1];
                
                }
        
        
        }

	/**
	 Given a string of x,y pairs ("0 0	0 1 0 2 1 0"), parses
	 the points into a TPoint[] array.
	 (Provided code)
	*/
	private static TPoint[] parsePoints(String string) {
		List<TPoint> points = new ArrayList<TPoint>();
		StringTokenizer tok = new StringTokenizer(string);
		try {
			while(tok.hasMoreTokens()) {
				int x = Integer.parseInt(tok.nextToken());
				int y = Integer.parseInt(tok.nextToken());
				
				points.add(new TPoint(x, y));
			}
		}
		catch (NumberFormatException e) {
			throw new RuntimeException("Could not parse x,y string:" + string);
		}
		
		// Make an array out of the collection
		TPoint[] array = points.toArray(new TPoint[0]);
		return array;
	}

/*public static void main(String args[])
{

 //Piece pyra2 = pyra.computeNextRotation(); // get rotation, slow way
 
 //Piece[] pieces = Piece.getPieces();	// the array of root pieces
 
 Piece s = new Piece(S1_STR);
 int[] s1 = s.getSkirt();
 
 
 
 for(int i=0;i<s1.length;i++)
 System.out.print(s1[i]);
 //System.out.print(stick.getWidth());
 //Piece stick2 = stick.fastRotation();	// get the next rotation, fast way
// System.out.print(stick2.getHeight());
// System.out.print(stick2.getWidth());
 //Piece stick3 = stick2.fastRotation();
 //System.out.print(stick3.getHeight());
// System.out.print(stick3.getWidth());
 
 
 

 
 
 

}

*/
}
